function B = produs(A,n)
	dim = size(A);
	
	if n == 0
		B = eye(dim);
		return 
	else if n == 1
		B = A;
		return 
	else if mod(n,2) == 0
		B = produs(strassen1(A,A),n/2);
		return 
		
	else 
		B = strassen1(A,produs(strassen1(A,A),(n-1)/2));
		return 
		
	end
	end
	end

endfunction

