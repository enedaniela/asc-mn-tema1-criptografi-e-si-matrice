function decrypt()
	%reads from file
	fidr = fopen('input1B', 'r');
	vn = fgetl(fidr);
	A = dlmread('key1B',SEP=' ',R0=1, C0=0);
	fidw = fopen('output1B', 'w');
	
	[n n] = size(A);
	%turns special char into pseudochar
	for i = 1:length(vn) 
		if(vn(i) == ' ')
			vn(i) = '`';
		elseif (vn(i) == '.')
			vn(i) = '{';
		elseif(vn(i) == "'")
			vn(i) = '|';
		endif
	end
	%turns all chars to lower-case
	vn = tolower(vn);
	% turns char to int
	vc = toascii(vn)- 96;
	% completes the array with zeros
	l = length(vc);
	w = mod(l,n);
	z = n-w;
	for i = l+1:l+z
		vc(i) = 0;
	endfor

	vc = reshape(vc.',n,[]');

	%calculates the new array
	A = GJ1(A);
	vi = A*vc;
	vi = mod(vi,29);

	vi = reshape(vi.',[],n');
	vi = reshape(vi.',1,[]');
	
	%turns the array of int into chars
	vix = setstr(vi+96);
	%turns the pseudochars into special chars
	for i = 1:length(vix)
		if(vix(i) == "`")
			vix(i) = " ";
		endif
	endfor	
	for i = 1:length(vix)
		if(vix(i) == "{")
			vix(i) = ".";
		endif
	endfor	
	for i = 1:length(vix)
		if(vix(i) == "|")
			vix(i) = "'";
		endif
	endfor
	
	fputs(fidw,vix);
	
	fclose(fidr);
	fclose(fidw);
endfunction
