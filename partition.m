function [A] = partition(A)
	
	[n n] = size(A);
%getting out of the recursive function
	if n == 1
		A = 1/A;
		return
	elseif n == 2
		A = 1/det(A)*[A(2,2) -A(1, 2); -A(2, 1) A(1, 1)];
		return
	end
%starts from the middle of the matrix
	if mod(n, 2) == 1
		m = (n - 1)/2;
	else
		m = n/2;
	end
%divides the submatrices A1 and A4 
	for i = m : -1 : 1
		A1 = A(1 : i, 1 : i);
%checks if A1 and A4 are invertible
		if det(A1) ~= 0
			A4 = A(i + 1 : n, i + 1 : n);
			if det(A4) ~= 0
				break
			end
		end
	end
%devides the submatrices A2 and A3
	A3 = A(1:i,i+1:n);
	A2 = A(i+1:n,1:i);
%apply the formulas
	B1 = partition(A1);
	B4 = partition(A4);
	X1 = partition(A1 - strassen1(A3,strassen1(B4,A2)));
	X2 = -strassen1(B4,strassen1(A2,X1));
	X4 = partition(A4 - strassen1(A2,strassen1(B1,A3)));
	X3 = -strassen1(B1,strassen1(A3,X4));
%rebuild the inverse
	A = [X1 X3; X2 X4];
end
		
