function [ I  ] = GJ1(A)

%upside-down	
[n n] = size(A);
I = eye(n);

for p = 1:n
	for k = 1:29
		w = mod(A(p,p)*k,29);
		if w == 1
			x = k;
			break;
		endif	
		
	endfor	

	A(p,:) = mod(A(p,:)*x,29);
	I(p,:) = mod(I(p,:)*x,29);


	for i = p+1:n
		I(i,:) = I(i,:) + mod(- A(i,p), 29)*I(p,:);
		A(i,:) = A(i,:) + mod(- A(i,p), 29)*A(p,:);
		
	end

end
A = mod(A,29);
I = mod(I,29);

%bottom-up
for p = n:-1:1
	for k = 1:29
		w = mod(A(p,p)*k,29);
		if w == 1
			x = k;
 		break;
		endif	
		
	endfor	
	A(p,:) = mod(A(p,:)*x,29);
	I(p,:) = mod(I(p,:)*x,29);

	for i = p-1:-1:1
		I(i,:) = I(i,:) + mod(- A(i,p), 29)*I(p,:);
		A(i,:) = A(i,:) + mod(- A(i,p), 29)*A(p,:);
		
	end

end
A = mod(A,29);
I = mod(I,29);

endfunction	
