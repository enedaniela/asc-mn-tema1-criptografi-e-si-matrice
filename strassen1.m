function [C] = strassen1(A, B)
[n n] = size(A);
%getting out of the recursive function
if n <= 32
	C = A*B;
	return
endif
%divides the matrix
pn = n/2;
A1 = A(1:pn, 1:pn);
A2 = A(1:pn, pn+1:n);
A3 = A(pn+1:n, 1:pn);
A4 = A(pn+1:n, pn+1:n);
B1 = B(1:pn, 1:pn);
B2 = B(1:pn, pn+1:n);
B3 = B(pn+1:n, 1:pn);
B4 = B(pn+1:n, pn+1:n);
%apply the formulas
M1 = strassen1(A1+A4,B1+B4);
M2 = strassen1(A3+A4,B1);
M3 = strassen1(A1,B2-B4);
M4 = strassen1(A4,B3-B1);
M5 = strassen1(A1+A2,B4);
%build the final matrix
C1 = M1+M4-M5+strassen1(A2-A4,B3+B4);
C2 = M3+M5;
C3 = M2+M4;
C4 = M1-M2+M3+strassen1(A3-A1,B1+B2);
C = [C1 C2;C3 C4];

endfunction

