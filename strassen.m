function  strassen(file_in, file_out)
	fid = fopen(file_in,'r');
	k = fscanf(fid, '%d', 1);
	A = dlmread(file_in,SEP=' ',R0=1, C0=0);
	[n m] = size(A);
	A = A(:,1:n);
	
	
	A = partition(A);
	
	p = 1;
	while p < n
		p = p * 2;
	endwhile

	A(p,p) = 0;

	A = produs(A,k);
	
	A = A(1:n,1:n);
	
	fid_out = fopen(file_out,'w');
	for i=1:size(A, 1)
    	fprintf(fid_out, '%f ', A(i,:));
    	fprintf(fid_out, '\n');
	end
	fclose(fid_out);
	fclose(fid);
endfunction


