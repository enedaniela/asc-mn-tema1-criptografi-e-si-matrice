function transposition()
	%reads from file
	fid = fopen('input1C', 'r');
	k = fscanf(fid, '%d', 1);
	n = fscanf(fid, '%d', 1);
	n = n+1;
	%deletes old output
	fidf = fopen('output1C','w');
	fclose(fidf);
	%opens file for writing with append option
	fidw = fopen('output1C', 'a');
	nr = 0;
	%reads until end of file
	while (! feof(fid))
		vn = fgetl(fid,1000);
	
	%turns special char into pseudochar
	for i = 1:length(vn) 
		if(vn(i) == ' ')
			vn(i) = '`';
		elseif (vn(i) == '.')
			vn(i) = '{';
		elseif(vn(i) == "'")
			vn(i) = '|';
		endif
	end
	%turns all chars to lower-case
	vn = tolower(vn);
	% turns char to int
	vn = toascii(vn)- 96;

	% creates the vector
	l = length(vn);
	w = mod(l,n-1);
	z = n-1-w;
	for i = l+1:l+z 
		vn(i) = 0; %completes with zeros
	end
	
	vn = reshape(vn.',n-1,[]');
	
	vn(n,:) = 1; % adds 1 at the and of every column

	% creates the transposition matrix
	A = eye(n);
	A(1:n,n) = k-1;
	k++;
	nr++;
	%writes the transposition matrix in the file
	if nr == 2
		save key1C A
	endif

	vn = A*vn;

	vn = vn(1:n-1,:);
	vn = reshape(vn.',[],n-1');
	vn = reshape(vn.',1,[]');
	vn = mod(vn,29);
	vn = vn+96;
	lung = length(vn);
	%turns int into char
	vn = setstr(vn(1:lung-z));
	%turns pseudochars into special chars
	for i = 1:length(vn)
		if(vn(i) == "`")
			vn(i) = " ";
		endif
	endfor	
	for i = 1:length(vn)
		if(vn(i) == "{")
			vn(i) = ".";
		endif
	endfor	
	for i = 1:length(vn)
		if(vn(i) == "|")
			vn(i) = "'";
		endif
	endfor
	%writs every 1000 chars in the file
	fputs(fidw,vn);
	endwhile

	fclose(fid);
	fclose(fidw);

endfunction


	